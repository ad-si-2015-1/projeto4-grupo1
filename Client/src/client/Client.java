/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import server.Server;

/**
 *
 * @author Augusto
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException {
        int numeroJogador;
        String resposta;
        String anagrama;
        String ip;
        Scanner s = new Scanner(System.in);
        
        System.out.println("Digite o ip do servidor: ");
        ip = s.nextLine();
        
        URL url = new URL("http://" + ip + ":9999/anagramaServer?wsdl");
        QName qname = new QName("http://server/", "ServerImplService");
        Service ws = Service.create(url, qname);
        Server server = ws.getPort(Server.class);
        
        numeroJogador = server.conectarJogador();
        System.out.println("Conectado! Aguarde até que os outros jogadores também se conectem.");
        anagrama = server.getAnagrama(numeroJogador);
        System.out.println("Pronto! Início do Jogo!\n" + "Resolva o anagrama: " + anagrama);
        
        while( !anagrama.equals("VENCEDOR") && !anagrama.equals("DESCLASSIFICADO") ){  
            resposta = s.nextLine();
            boolean acertou = server.responder(numeroJogador, resposta);
            if(acertou){
                System.out.println("Resposta CORRETA!!!\nAguardando próxima rodada...");
                anagrama = server.getAnagrama(numeroJogador);
            } else{
                System.out.println("Resposta INCORRETA.\n Tente novamente:\n");
            }
        }
        
        if( anagrama.equals("VENCEDOR") ){
            System.out.println("Você venceu o jogo!");
        }
        if( anagrama.equals("DESCLASSIFICADO") ){
            System.out.println("Você está desclassificado!");
        }
    }
}
