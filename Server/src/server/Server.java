/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 *
 * @author Augusto
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface Server {
    @WebMethod
    boolean responder(int numJogador, String resposta);
    @WebMethod
    int conectarJogador();
    @WebMethod
    String getAnagrama(int numJogador);
}
