/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.util.Scanner;
import javax.xml.ws.Endpoint;

/**
 *
 * @author Augusto
 */
public class ServerPublisher {
    public static void main(String[] args){
        int numJogadores;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("ANAGRAMA");
        System.out.println("Iniciando...");
        System.out.println("\n\nPor favor indique o número de jogadores: ");
        
        numJogadores = sc.nextInt();
        
        while(numJogadores <= 1){
            System.out.println("É preciso pelo menos 2 jogadores. Digite outro valor: ");
            numJogadores = sc.nextInt();
        }

        System.out.println("O jogo se iniciará quando os "+numJogadores+" se conectarem.");
        GerenciadorPalavras gerPalavras = new GerenciadorPalavras();
        
        Endpoint.publish( "http://127.0.0.1:9999/anagramaServer", new ServerImpl(gerPalavras, numJogadores) );
    }
}
