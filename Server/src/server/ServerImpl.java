/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;

/**
 *
 * @author Augusto
 */
@WebService(endpointInterface="server.Server")
public class ServerImpl implements Server{
    GerenciadorPalavras gerPalavras;
    HashMap<Integer, Jogador> jogadores;
    String palavra;
    String anagrama;
    int numJogadores;
    int classificados;
    boolean novaRodada = false;
    private Jogador vencedor = null;
    

    public ServerImpl(GerenciadorPalavras gerPalavras, int numJogadores) {
        this.gerPalavras = gerPalavras;
        jogadores = new HashMap<>();
        this.numJogadores = numJogadores;
    }
    
    @Override
    public boolean responder(int numJogador, String resposta) {
        boolean retorno = ( resposta.equals(this.palavra) );
        
        if(retorno && this.jogadores.size() == 2 && this.vencedor == null){
            this.vencedor = jogadores.get( (Integer) numJogador );
            vencedor.setStatus("VENCEDOR");
        } else if( this.jogadores.size() - this.classificados == 1 || vencedor != null){
            jogadores.get( (Integer) numJogador ).setStatus("DESCLASSIFICADO");
        } else if(retorno){
            jogadores.get( (Integer) numJogador ).setStatus("CLASSIFICADO");
            this.classificados ++ ;
        }
        
        return retorno;
    }

    @Override
    public int conectarJogador() {
        if( jogadores.size() < numJogadores ){
            int numJogador = jogadores.size() + 1;
            Jogador jogador = new Jogador(numJogador);
            this.jogadores.put(numJogador, jogador);
            
            if( jogadores.size() == numJogadores ){
                this.iniciarJogo();
            }
            
            return numJogador;
        }
        return 0;
    }

    @Override
    public String getAnagrama(int numJogador) {
        String statusJogador = this.jogadores.get((Integer) numJogador).getStatus();
        
        if( statusJogador.equals("DESCLASSIFICADO") ){
            this.jogadores.remove( (Integer) numJogador );
            if(vencedor == null) this.iniciarNovaRodada();
            return statusJogador;
        } else if(statusJogador.equals("VENCEDOR")){
            this.jogadores.remove( (Integer) numJogador );
            System.out.println("Jogador "+ numJogador +" venceu!\nFIM DO JOGO!");
            return statusJogador;
        }
        
        while( !this.novaRodada ){
            try {
                Thread thread = new Thread();
                thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ServerImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.anagrama;
    }
    
    
    public void iniciarJogo(){
        this.iniciarNovaRodada();
        System.out.println("Todos os jogadores se conectaram.\nJogo INICIADO.");
    }
    
    public void iniciarNovaRodada(){
        this.novaRodada = true;
        this.classificados = 0;
        String[] palavraAnagrama = gerPalavras.getPalavraAnagrama();
        this.palavra = palavraAnagrama[0];
        this.anagrama = palavraAnagrama[1];
        System.out.println("Iniciando nova rodada...");
        System.out.println("Palavra: " + this.palavra);
        System.out.println("Anagrama: " + this.anagrama);
    }
}
